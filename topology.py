import matplotlib.pyplot as plt
import numpy as np
import csv
import pdb
from PIL import Image
import math
import random
import pdb

time = []
opn = []
high = []
low = []
close = []
volB = []
volD = []
avg = []

# Time is formatted in Unix
with open('CoinBaseData.csv','r', newline='') as dataFile:
    next(dataFile)
    plot = csv.reader(dataFile, delimiter=',')
    for row in plot:
        time.append(float(row[0]))
        opn.append(float(row[1]))
        high.append(float(row[2]))
        low.append(float(row[3]))
        close.append(float(row[4]))
        volB.append(float(row[5]))
        volD.append(float(row[6]))
        avg.append(float(row[7]))

# Create a 2D Matrix of Average Price and Time
#mat = np.stack((avg,time))
#mat = mat.T
#print(mat)

num_cells = len(avg)
r = []
g = []
b = []
for i in range(num_cells):
    r.append(random.randrange(256))
    g.append(random.randrange(256))
    b.append(random.randrange(256))
image = Image.new("RGB",(len(avg),len(time)))
putpixel = image.putpixel
imgx,imgy = image.size
for y in range(imgy):
    for x in range(imgx):
        dmin = math.hypot(imgx-1,imgy-1)
        j = -1
        for i in range(num_cells):
            d = math.hypot((time[i] - x),(avg[i] - y))
            if d < dmin:
                dmin = d
                j = i
        putpixel((x,y),(r[j],g[j],b[j]))
image.save("VD.png", "PNG")
image.show()








